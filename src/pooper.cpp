#include "main.h"
#include "base.h"
#include "intake.h"
#include "pooper.h"

pros::Mutex pooper_mutex;

pros::Motor pooper_mtr(POOPER_MOTOR_PORT, REVERSE);


void pooperControl(void* param) {
  //manage pooper
  std::uint32_t now = pros::millis();
  double maxTimeBeforeSecondClick = 200;
  double pooperClickTime = 0;

  int x;
  int b;

  while (true) {
    x = master.get_digital(DIGITAL_X);
    b = master.get_digital(DIGITAL_B);
    if (pooper_mutex.take(MUTEX_WAIT_SHORT)) {
      if (x) { //rollers in
        pooperMove(127);
      } else if (b) {
        pooperMove(-127);
      } else { //shut off else
        pooperMove(5);
      }
      pooper_mutex.give();
     }
   pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
 }
}

void pooperMove(int pooperPower) {
  pooper_mtr = pooperPower;
}

void pooperControlTaskInit() {
pros::Task pooper_task(pooperControl,(void*)"DUMMY");
}
