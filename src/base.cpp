#include "main.h"
#include "base.h"

pros::Mutex base_mutex;


pros::Motor right_front_mtr(FRONT_RIGHT_DRIVE_PORT, REVERSE);
pros::Motor left_front_mtr(FRONT_LEFT_DRIVE_PORT);
pros::Motor right_back_mtr(BACK_RIGHT_DRIVE_PORT, REVERSE);
pros::Motor left_back_mtr(BACK_LEFT_DRIVE_PORT);
pros::Motor right_middle_mtr(MIDDLE_RIGHT_DRIVE_PORT);
pros::Motor left_middle_mtr(MIDDLE_LEFT_DRIVE_PORT, REVERSE);
pros::Motor test_mtr_port(TEST_PORT);


//testing motors:
// right_front_mtr 12 drifting @4rpm 20.09
// left_front_mtr 20 20.8
// right_back_mtr 14 drifting 20.11
// left_back_mtr 19 21.00
// right_middle_mtr 5 21.99
// left_middle_mtr 6 20.88

void baseControl(void* param) {
  //manage base
  std::uint32_t now = pros::millis();
  setBaseCurrentLimit(2500);

  while (true) {
    if (base_mutex.take(MUTEX_WAIT_SHORT)) {
      int left = master.get_analog(ANALOG_LEFT_Y);
      int right = master.get_analog(ANALOG_RIGHT_Y);

      left_back_mtr = left;
      right_back_mtr = right;
      left_front_mtr = left;
      right_front_mtr = right;
      left_middle_mtr = left;
      right_middle_mtr = right;

      // getMotorTelemtry(FRONT_RIGHT_DRIVE_PORT);
      // getMotorTelemtry(MIDDLE_RIGHT_DRIVE_PORT);
      // getMotorTelemtry(BACK_RIGHT_DRIVE_PORT);
      // getMotorTelemtry(FRONT_LEFT_DRIVE_PORT);
      // getMotorTelemtry(MIDDLE_LEFT_DRIVE_PORT);
      // getMotorTelemtry(BACK_LEFT_DRIVE_PORT);

			base_mutex.give();
    }
    pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
  }
}

void baseControlTaskInit() {
  pros::Task base_task(baseControl,(void*)"DUMMY");
}

void baseMove(int power) {
  left_back_mtr = power;
  right_back_mtr = power;
  left_front_mtr = power;
  right_front_mtr = power;
  left_middle_mtr = power;
  right_middle_mtr = power;
}

void getMotorTelemtry(int port) {\

  int outputPower;
  int output_velocity;
  int current_draw;
  int efficiency;
  output_velocity = pros::c::motor_get_actual_velocity(port);
  std::cout << "Output Velocity = " << (output_velocity) << std::endl;
  current_draw = pros::c::motor_get_current_draw(port);
  std::cout << "Current Draw = " << (current_draw) << std::endl << std::endl;
}

void testMotorEfficiency(int port) { //outputs data for motor efficiency

  int left;

  while (true) {
    left = master.get_digital(DIGITAL_LEFT);

    if (left) {
      int n=0;
      int elapsedTime = 0;
      double avgEfficiency;
      double efficiency [5]; //define motor efficiency array
      std::uint32_t now = pros::millis();

      while (elapsedTime < 5100 /*Spin for 5 sec*/) {
        test_mtr_port = 127; //start test motor
        efficiency [n] = pros::c::motor_get_actual_velocity(port);
        n++;
        elapsedTime = pros::millis() - now;
        pros::delay(1000);
      }

      test_mtr_port = 0; //stop test motor
      avgEfficiency = (efficiency[0] + efficiency[1] + efficiency[2] + efficiency[3] + efficiency[4])/5;
      std::cout << "Average Efficiency = " << (avgEfficiency) << std::endl;
      left = false;

    
      // double totalVelocity = 0;
      // double avgVelocity;
      // test_mtr_port = 127; //start test motor
      // for (n=0; n<100; n++) {
      //   totalVelocity += pros::c::motor_get_actual_velocity(port);
      // }
      // avgVelocity = totalVelocity/n;
      // test_mtr_port = 0; //stop test motor
      // std::cout << "Average Velocity = " << (avgVelocity) << std::endl;
      // left = false;



    }
  }
}

void setBaseCurrentLimit(int limit) {
  right_front_mtr.set_current_limit(limit);
  left_front_mtr.set_current_limit(limit);
  right_back_mtr.set_current_limit(limit);
  left_back_mtr.set_current_limit(limit);
  right_middle_mtr.set_current_limit(limit);
  left_middle_mtr.set_current_limit(limit);
}
