#include "main.h"
#include "base.h"
#include "intake.h"
#include "fly_wheel.h"

pros::Mutex fly_wheel_mutex;

pros::Motor fly_wheel_mtr(FLY_WHEEL_MOTOR_PORT, REVERSE);


void flyWheelControl(void* param) {
  //manage fly_wheel
  std::uint32_t now = pros::millis();
  double maxTimeBeforeSecondClick = 200;
  double fly_wheelClickTime = 0;

  int right1;
  int y;
  int right2;

  while (true) {
    right1 = master.get_digital(DIGITAL_R1);
    right2 = master.get_digital(DIGITAL_R2);
    y = master.get_digital(DIGITAL_Y);
    if (fly_wheel_mutex.take(MUTEX_WAIT_SHORT)) {
      if (right1 || right2) { //shoot
        flyWheelMove(127);
      } else if (y) { //flush
        flyWheelMove(-127);
      } else { //shut off else
        flyWheelMove(0);
      }
      fly_wheel_mutex.give();
     }
   pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
 }
}

void flyWheelMove(int flyWheelPower) {
  fly_wheel_mtr = flyWheelPower;
}

void flyWheelControlTaskInit() {
pros::Task fly_wheel_task(flyWheelControl,(void*)"DUMMY");
}
