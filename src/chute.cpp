#include "main.h"
#include "base.h"
#include "intake.h"
#include "chute.h"

pros::Mutex chute_mutex;

pros::Motor chute1_mtr(CHUTE1_MOTOR_PORT);
pros::Motor chute2_mtr(CHUTE2_MOTOR_PORT, REVERSE);


void chuteControl(void* param) {
  //manage chute
  std::uint32_t now = pros::millis();
  double maxTimeBeforeSecondClick = 200;
  double chuteClickTime = 0;


  int right1;
  int right2;
  int left1;
  int left2;
  int y;

  while (true) {
    right1 = master.get_digital(DIGITAL_R1);
    right2 = master.get_digital(DIGITAL_R2);
    left1 = master.get_digital(DIGITAL_L1);
    left2 = master.get_digital(DIGITAL_L2);
    y = master.get_digital(DIGITAL_Y);
    if (chute_mutex.take(MUTEX_WAIT_SHORT)) {
      if (right1 || right2 || left1 || left2) { //rollers in
        chuteMove(127);
      } else if (y) {
        chuteMove(-127);
      } else {
        chuteMove(0);
      }
      chute_mutex.give();
     }
   pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
 }
}

void chuteMove(int chutePower) {
  chute1_mtr = chutePower;
  chute2_mtr = chutePower;
}

void chuteControlTaskInit() {
pros::Task chute_task(chuteControl,(void*)"DUMMY");
}
