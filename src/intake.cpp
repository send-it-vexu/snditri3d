#include "main.h"
#include "base.h"
#include "intake.h"

pros::Mutex intake_mutex;

pros::Motor left_roller_mtr(LEFT_ROLLER_MOTOR_PORT, REVERSE);
pros::Motor right_roller_mtr(RIGHT_ROLLER_MOTOR_PORT);
pros::Motor top_roller_mtr(TOP_ROLLER_MOTOR_PORT);


void intakeControl(void* param) {
  //manage intake
  std::uint32_t now = pros::millis();
  double maxTimeBeforeSecondClick = 200;
  double intakeClickTime = 0;

  int left1;
  int left2;

  while (true) {
    left1 = master.get_digital(DIGITAL_L1);
    left2 = master.get_digital(DIGITAL_L2);
    if (intake_mutex.take(MUTEX_WAIT_SHORT)) {
      if (left1) { //rollers in
        intakeMove(127);
        indexMove(127);
      } else if (left2) {
        intakeMove(-127);
        indexMove(127);
      } else { //shut off else
        intakeMove(0);
        indexMove(0);
      }
      intake_mutex.give();
     }
   pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
 }
}

void intakeMove(int sideRollerPower) {
  right_roller_mtr = sideRollerPower;
  left_roller_mtr = sideRollerPower;
}
void indexMove(int indexPower) {
  top_roller_mtr = indexPower;
}

//R2single = intakeIn
//R2double = intakeOut
//R1single = fireCatapult & intakeStop
//L1 = scrapertoggle
//L2 = intakeStop

void intakeControlTaskInit() {
pros::Task intake_task(intakeControl,(void*)"DUMMY");
}
