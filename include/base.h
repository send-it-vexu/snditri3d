#ifndef _BASE_H_
#define _BASE_H_

#include "api.h"
#include "main.h"

#ifdef __cplusplus
extern "C" {

extern pros::Mutex base_mutex;

//port numbers
#define FRONT_RIGHT_DRIVE_PORT 12
#define FRONT_LEFT_DRIVE_PORT 20
#define BACK_RIGHT_DRIVE_PORT 14
#define BACK_LEFT_DRIVE_PORT 19
#define MIDDLE_LEFT_DRIVE_PORT 5
#define MIDDLE_RIGHT_DRIVE_PORT 6
#define TEST_PORT 11


#define DEFAULT_CURRENT 2500


extern pros::Motor left_back_mtr;
extern pros::Motor right_back_mtr;
extern pros::Motor left_front_mtr;
extern pros::Motor right_front_mtr;
extern pros::Motor left_middle_mtr;
extern pros::Motor right_middle_mtr;
extern pros::Motor test_mtr_port;
// extern pros::Controller master(pros::E_CONTROLLER_MASTER);

void getMotorTelemtry(int port);
void baseMove(int power);
void baseControlTaskInit();
void setBaseCurrentLimit(int limit);
void testMotorEfficiency(int port);


#endif
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/**
 * You can add C++-only headers here
 */

#endif

#endif  // _BASE_H_
