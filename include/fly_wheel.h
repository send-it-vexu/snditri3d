#ifndef _FLY_WHEEL_H_
#define _FLY_WHEEL_H_

#include "api.h"

#ifdef __cplusplus
extern "C" {

extern pros::Mutex fly_wheel_mutex;



#define FLY_WHEEL_MOTOR_PORT 4


extern pros::Motor fly_wheel_mtr;


void flyWheelControlTaskInit();
void flyWheelMove(int flyWheelPower);



#endif
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/**
 * You can add C++-only headers here
 */

#endif

#endif  // _FLY_WHEEL_H_
