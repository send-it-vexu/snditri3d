#ifndef _INTAKE_H_
#define _INTAKE_H_

#include "api.h"

#ifdef __cplusplus
extern "C" {

extern pros::Mutex intake_mutex;


#define TOP_ROLLER_MOTOR_PORT 3
#define LEFT_ROLLER_MOTOR_PORT 18
#define RIGHT_ROLLER_MOTOR_PORT 17


//port 10 fried


extern pros::Motor left_roller_mtr;
extern pros::Motor right_roller_mtr;
extern pros::Motor top_roller_mtr;
extern pros::Motor chute1_mtr;
extern pros::Motor chute2_mtr;


void intakeControlTaskInit();
void intakeMove(int sideRollerPower);
void chuteMove(int chutePower);
void indexMove(int indexPower);


#endif
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/**
 * You can add C++-only headers here
 */

#endif

#endif  // _INTAKE_H_
