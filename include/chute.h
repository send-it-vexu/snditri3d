#ifndef _CHUTE_H_
#define _CHUTE_H_

#include "api.h"

#ifdef __cplusplus
extern "C" {

extern pros::Mutex chute_mutex;



#define CHUTE1_MOTOR_PORT 13
#define CHUTE2_MOTOR_PORT 15

extern pros::Motor chute_mtr;


void chuteControlTaskInit();
void chuteMove(int chutePower);



#endif
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/**
 * You can add C++-only headers here
 */

#endif

#endif  // _CHUTE_H_
