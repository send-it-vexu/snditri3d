#ifndef _POOPER_H_
#define _POOPER_H_

#include "api.h"

#ifdef __cplusplus
extern "C" {

extern pros::Mutex pooper_mutex;



#define POOPER_MOTOR_PORT 5


extern pros::Motor pooper_mtr;


void pooperControlTaskInit();
void pooperMove(int pooperPower);



#endif
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/**
 * You can add C++-only headers here
 */

#endif

#endif  // _POOPER_H_
